set table "main.curve.table"; set format "%.5f"
 f(x,y) = y**2 - x**3 + x; set xrange [-5:5]; set yrange [-3:3]; set view 0,0; set isosample 200,200; set size square; set cont base; set cntrparam levels incre 0,0.1,0; unset surface; splot f(x,y) 
