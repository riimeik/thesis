set terminal latex
set output 'main-gnuplottex-fig1.tex'
set grid
set title 'gnuplottex test $e^x$'
set ylabel '$y$'
set xlabel '$x$'
plot exp(x) with linespoints
