\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {section}{\numberline {2}Mathematical preliminaries}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Groups}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Rings and fields}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Field extensions}{5}{subsection.2.3}
\contentsline {section}{\numberline {3}Elliptic curves}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Overview}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The group law}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Explicit formulas}{8}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3}Pairing-friendly curves}{8}{subsection.3.3}
\contentsline {section}{\numberline {4}Bilinear pairings}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Overview}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The ate pairing}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Miller functions, algorithm for ate}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Optimisations}{9}{subsection.4.4}
\contentsline {section}{\numberline {5}Parallel computing model for pairings}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}OpenCL}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Algorithms}{10}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Proposed parallel algorithm}{10}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusion}{11}{section.6}
\contentsline {section}{References}{12}{section*.4}
\contentsline {section}{Appendix}{13}{section*.5}
\contentsline {subsection}{I. Glossary}{13}{section*.6}
\contentsline {subsection}{II. Licence}{14}{section*.7}
